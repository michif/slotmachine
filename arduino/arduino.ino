
#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008

bool debug=false;

bool leverstateBefore;

int sensorPin = A3;    // select the input pin for the potentiometer
int analogValBefore=0;



// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
// Newer Ethernet shields have a MAC address printed on a sticker on the shield

byte mac[] = {  
  0x90,0xA2,0xDA,0x0D,0xC3,0x03};

byte myIp[]  = { 
 192, 168, 1, 177};
  
  
 

unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet,

IPAddress remoteIp;
int port;

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;
bool newMessage=false;

char replytrue[]="1";  
char replyfalse[]="0";  

char  ReplyBuffer[] = "acknowledged";       // a string to send back



void setup() {
  //Setup Serial. Only for debugging
  Serial.begin(9600);
  Serial.print("Setup ");


  // start Ethernet and UDP:
  Ethernet.begin(mac,myIp);
  digitalWrite (10, HIGH);

  delay(1000);
  Serial.println("connecting...");
  // print your local IP address:
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print("."); 
  }
  Serial.println();
  delay(1000);
  Udp.begin(localPort);  
  
  
  sendMessage();
}


void loop() {

   // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i =0; i < 4; i++)
    {
      Serial.print(remote[i], DEC);
      if (i < 3)
      {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    // send a reply, to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer);
    Udp.endPacket();
  }
  delay(10);
  
  
 // if(readLever()){
     // sendMessage();
 // }
  
  
  
  
  
  /*
  
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    //Get remoteserver
    remoteIp = Udp.remoteIP();
    port=Udp.remotePort();
    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    newMessage=true;
  }


  if(newMessage){
    if(debug)Serial.println(packetBuffer);
    newMessage=false;
  }

*/

}


void sendMessage(){
Udp.beginPacket(remoteIp, port);
Udp.write(ReplyBuffer);
Udp.endPacket();


}



bool readLever(){
  bool leverstate=false; 
  bool change=false;
  int analogVal = analogRead(sensorPin);    

  Serial.println(analogVal);

  if( analogVal> 50 && analogValBefore<analogVal-50){
    leverstate=true;
  }


  if(leverstate!=leverstateBefore && leverstate==true){ //prevent firing on the way back...
    change=true;
    //   Serial.println("change");
    //if(!debug)Udp.beginPacket(remoteIp, port);
  //  if(leverstate){

    //}
    //else{
      //if(!debug)Udp.write(replyfalse);
   // }
    //if(!debug)Udp.endPacket();
  }

  leverstateBefore=leverstate;
  analogValBefore=analogVal;
  return change;//leverstate;

}



