//
//  slotmachinecontroller.h
//  slotmachine
//
//  Created by Michael Flueckiger on 12.05.15.
//
//

#ifndef __slotmachine__slotmachinecontroller__
#define __slotmachine__slotmachinecontroller__

#include <stdio.h>

#include "ofMain.h"
#include "wheel.h"
#include "UdpManger.h"
//#include "movingObject.h"

#include "winmanager.h"

#include "ofxGui.h"



#define TURNING 300
#define STOP 400
#define WIN 200
#define SCREENSAVING 500


//for convenience
#define APPC Slotmachinecontroller::getInstance()

class Slotmachinecontroller {
    
public:
    static Slotmachinecontroller* getInstance();
    
    // OF  Functions
    void setup();
    void update();
    void draw();
    
    
    
    ofTrueTypeFont  mono;
    string typeStr;
    
    
    WinManager *winmanager;
    
    pricesStruct myPrice;
    
    vector<Wheel>wheels;

    
    void addKeyListener ();
    void removeKeyListener();


    void turnOnKeyListener();
    void turnOffKeyListener();
    bool isKeyListening();
    bool bAddedKeyListener;


    //We need to declare all this mouse events methods to be able to listen to mouse events.
    //All this must be declared even if we are just going to use only one of this methods.
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    void keyPressed(ofKeyEventArgs &e);

    

    void touchDown(ofTouchEventArgs & touch);
    void touchMoved(ofTouchEventArgs & touch);
    void touchUp(ofTouchEventArgs & touch);
    void touchDoubleTap(ofTouchEventArgs & touch);
    void touchCancelled(ofTouchEventArgs & touch);

    
    void castDice();
    void setWinningThreshold(float threshold);
    float getWinningThreshold();
    
    void wheelStopped(int & i);
    
    int totalNumberOfImages;
    
    
    vector<ofImage>icons;
    vector<ofImage>icons_sw;

    
    ofImage myImage;
    ofImage overlay;
    ofImage mergedImage;
    ofImage croppedImage;
    
    ofFbo generatedTicket; // with alpha
    ofPixels pix ;
    
    string actualName;
    string generateTicket(int t_number, string _sort);
    
    
    void startWinningAnimation();
    void stopWinningAnimation();
    void winningAnimation();
    bool bIsWinningAnimation;
    
    
    void minSliderChanged(int &minSpeedSlider);
    void maxSliderChanged(int &maxSpeedSlider);

    
    //MovingObject mObject;
    
private:
    
    
    ofParameter<int>  minSpeedGui;
    ofParameter<int>  maxSpeedGui;
    
    ofxIntSlider minSpeedSlider;
    ofxIntSlider maxSpeedSlider;


    ofxPanel gui;
    
    bool bIsWinning;
    float winningThreshold;
    
    
    Slotmachinecontroller ();
    static Slotmachinecontroller* instance;
    
    UdpManager *udp_manager;
    
    bool bHasSuper;
    bool bHasSpecial;
    bool bHasNormal;
    
    string random_string(int length);

    int minSpeed;
    int maxSpeed;
    
};

#endif /* defined(__slotmachine__slotmachinecontroller__) */
