//
//  Colorswatch.cpp
//  emptyExample
//
//  Created by Michael Flueckiger on 30.07.12.
//  Copyright (c) 2012 Ich. All rights reserved.
//

#include <iostream>
#include "Colorswatch.h"

Colorswatch::Colorswatch(){
    bColorUpdated=false;
    myColor.set(255,255,255);
    bIsfadeing=false;
    reachedValue=0.01;
    fadeSpeed=0.5;
    fadetime=1;
    
};

Colorswatch::~Colorswatch(){
};

void Colorswatch::draw(){

};
void Colorswatch::update(){
    //myColor;
    
    
    ofColor tempc;
    tempc=actualColor;
    
    
    if(bIsreaching){
        tempc=fadeTo();
    //}else if(isblinking){
     //   setGeneralLerpSpeed(FAST_LERP);
     //   tempc=blinking();
    //}else if(isfeedbacking){
     //   setGeneralLerpSpeed(FAST_LERP);
     //   tempc=feedbacking();
   // }else if(islerping){
    //    tempc=lerpTo();
   // }else if(iswavecolor){
     //   tempc=myWaveColor;
    }else if(bIsfadeing){
        tempc=fadeTo();
    }
    actualColor=tempc;
    
    if(myColorBefore!=actualColor){
        bColorUpdated=true;
    }
    myColorBefore=actualColor;
};

void Colorswatch::setColor(ofColor col){
    myColor=col;
    myTargetColor=col;
    actualColor=col;

};



void Colorswatch::setSecondColor(ofColor col){
    mySecondColor=col;
 
    
};

ofColor Colorswatch::getActualColor(){
    return actualColor;
};

ofColor Colorswatch::getSetColor(){
    return myColor;
};


bool Colorswatch::isFadeing(){
    return bIsfadeing;
}

void Colorswatch::startFadeing(){
    bIsfadeing=true;
    then=ofGetElapsedTimeMillis();
    
}

void Colorswatch::stopFadeing(){
    bIsfadeing=false;
    bIsreaching=false;
    
    if(bGoback){
      setFadeColor(mySetColor,fadetime);
    }
    
}

void Colorswatch::setFadeColor(ofColor _col, float _fadetime, bool _changecolor, bool _reaching, bool _goback){
    if(!bIsreaching){
        bool changecolor=_changecolor;
        bIsreaching=_reaching;
        bGoback=_goback;
        
        if(bIsreaching)bReached=false;
        myTargetColor=_col;
        
        if(changecolor){
            mySetColor=myTargetColor;
        }
        fadetime=_fadetime;
        startFadeing();
    }else return;
}


ofColor Colorswatch::fadeTo(){
    
    ofColor fade=myColor;
    ofColor targetCol=myTargetColor;
    float fadefact;
    
    now=ofGetElapsedTimeMillis();
    float temp=now-then;
  //  cout<<temp<<" "<<fadetime<<endl;
    
    if(temp>fadetime){
        fade=myTargetColor;
        myColor=fade;
        stopFadeing();
    }else{
        fadefact=temp/fadetime;
        fade.lerp(targetCol,fadefact);
    }
    return fade;
}



void Colorswatch::startFeedbacking(){
    bIsfeedbacking=true;
    feedbackthen=ofGetElapsedTimeMillis();
    feedbackcounter=0;
}

ofColor Colorswatch::feedbacking(){
    ofColor feedback;
    
    if(bFeedbackingishigh){
        feedback=myColor;
    }else{
        feedback=myFeedbackColor;
    }
    
    float now=ofGetElapsedTimeMillis();
    
    if(now-then>feedbackfrequenzy){
        bFeedbackingishigh=!bFeedbackingishigh;
        feedbackthen=now;
        feedbackcounter++;
    }
    
    if (feedbackcounter>maxfeedback){
        stopFeedbacking();
    }
    return feedback;
}

void Colorswatch::stopFeedbacking(){
    bIsfeedbacking=false;
}

bool Colorswatch::isFeedbacking(){
    return bIsfeedbacking;
}



void Colorswatch::setFeedbackColor(ofColor col){

    myFeedbackColor.set(col);

}

