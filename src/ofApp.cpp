#include "ofApp.h"
#include "slotmachinecontroller.h"


//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);
    
    ofSetVerticalSync( true );
    ofHideCursor();


    //APPController Singleton
    APPC->setup();
    ofBackground(255);
    
    
  

}

//--------------------------------------------------------------
void ofApp::update(){
    //update Appcontroller
    APPC->update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    //update Appcontroller
    APPC->draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if(isdigit(key)) {
      //  cout << "digit"<<(int)(key - '0') << endl;
    }
    
    if (key =='d'){
        APPC->castDice();
        }

    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
