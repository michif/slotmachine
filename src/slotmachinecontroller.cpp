
//  slotmachinecontroller.cpp
//  slotmachine
//
//  Created by Michael Flueckiger on 12.05.15.
//
//

#include "slotmachinecontroller.h"


Slotmachinecontroller* Slotmachinecontroller::instance = 0;

Slotmachinecontroller* Slotmachinecontroller::getInstance() {
    if (!instance) {
        instance = new Slotmachinecontroller();
    }
    return instance;
}



Slotmachinecontroller :: Slotmachinecontroller ()
{
 cout << "creating Slotmachinecontroller" << endl;
    
}




//--------------------------------------------------------------
void Slotmachinecontroller::setup(){
   // ofAddListener(ofEvents().keyPressed, this, &Slotmachinecontroller::keyPressed);
    turnOnKeyListener();
    
//    minSpeedSlider.addListener(this, &Slotmachinecontroller::minSliderChanged);

    minSpeed=20;
    maxSpeed=25;
    
  
    
    
    // change default sizes for ofxGui so it's usable in small/high density screens
    ofxGuiSetFont("frabk.ttf",20,true,true);
    
    // change default sizes for ofxGui so it's usable in small/high density screens
    ofxGuiSetTextPadding(10);
    //ofxGuiSetDefaultWidth(800);
    ofxGuiSetDefaultWidth(600);
    
    ofxGuiSetDefaultHeight(40);
    
    
    
    
    
    gui.setup("panel","name",0,500); // most of the time you don't need a name but don't forget to call setup
    gui.add(minSpeedSlider.setup( "minspeed", 20, 0, 80 ));
    gui.add(maxSpeedSlider.setup( "maxspeed", 25, 0, 80 ));


    gui.loadFromFile("GUIsettings.xml");
    
    
    
    udp_manager=new UdpManager();
    
    winmanager= new WinManager();
    winmanager->setup();
    
    bIsWinning=false;
    winningThreshold=0.5;
    
   
    
    //go through and print out all the paths
    for(int i = 0; i < 4; i++){
        Wheel w;
        wheels.push_back(w);
        wheels.back().setId(i);
        wheels.back().setup();
        wheels.back().setPosition(ofVec3f(i*240,0,i*10));
        wheels.back().setSpeed(minSpeed,maxSpeed);
        
    }

    minSpeedSlider.addListener(this,&Slotmachinecontroller::minSliderChanged);
    maxSpeedSlider.addListener(this,&Slotmachinecontroller::maxSliderChanged);
    ofAddListener(MovingObject::targetObjectStopped, this, &Slotmachinecontroller::wheelStopped);

    	//ofAddListener(mObject.targetObjectReached,this,&Slotmachinecontroller::wheelStopped);
    
    
    myImage.loadImage("ticket_leer.jpg");
    mergedImage.allocate(myImage.width, myImage.height, OF_IMAGE_COLOR);
    croppedImage.allocate(myImage.width, myImage.height, OF_IMAGE_COLOR);
    
    generatedTicket.allocate(myImage.width, myImage.height, GL_RGBA32F_ARB);
    generatedTicket.begin();
    ofClear(255,255,255, 0);
    generatedTicket.end();
    
    //some path, may be absolute or relative to bin/data
    string path = "icons/";
    ofDirectory dir(path);
    //only show png files
    dir.allowExt("png");
    //populate the directory object
    dir.listDir();
    
    
    totalNumberOfImages=dir.numFiles();
    cout<<"------- totalNumberOfImages ------- "<<totalNumberOfImages<<endl;
    
    //go through and print out all the paths
    for(int i = 0; i < dir.numFiles(); i++){
        
        // add the image to the vector
        string filePath = dir.getPath(i);
        cout<<"loading"<<filePath<<endl;
        ofImage img;
        icons.push_back(img);
        icons.back().loadImage(filePath);
        
        //icons.push_back(img ofImage.loadImage(dir.getPath(i)));
        
    }
    
    
    //some path, may be absolute or relative to bin/data
    //some path, may be absolute or relative to bin/data
    string path_sw = "icons_Sw/";
    ofDirectory dir_sw(path_sw);
    //only show png files
    dir_sw.allowExt("png");
    //populate the directory object
    dir_sw.listDir();
    
    
    totalNumberOfImages=dir_sw.numFiles();
    cout<<"------- totalNumberOfImages ------- "<<totalNumberOfImages<<endl;
    
    //go through and print out all the paths
    for(int i = 0; i < dir_sw.numFiles(); i++){
        
        // add the image to the vector
        string filePath = dir_sw.getPath(i);
        ofImage img;
        icons_sw.push_back(img);
        icons_sw.back().loadImage(filePath);
    }


    
     bHasSuper =true;
     bHasSpecial =true;
     bHasNormal=true;
    
    
    
    mono.loadFont("FoundersGroteskMono-Regular.otf", 50,false);
    mono.setLineHeight(18.0f);
    mono.setLetterSpacing(1.037);
        
}



//--------------------------------------------------------------
void Slotmachinecontroller::update(){
    //__UDP ___________________________
    udp_manager->update();
    
    for(int i = 0; i < wheels.size(); i++){
        wheels[i].update();
    }
    
    
  
}


//--------------------------------------------------------------
void Slotmachinecontroller::draw(){
    
    for(int i = 0; i < wheels.size(); i++){
        wheels[i].draw();
        
        
    }
    
    
    ofFill();
    ofRect(0, 360, ofGetWidth(), ofGetHeight()-360);
    ofRect(4*240, 0, ofGetWidth()-4*240, ofGetHeight());
    
    gui.draw();

}


//KEY LISTENER
//--------------------------------------------------------------
void Slotmachinecontroller::keyPressed(ofKeyEventArgs &e){
    //int key=e.key;
    char* volatile p = 0;

        if(isdigit(e.key)) {
            
            for(int i = 0; i < wheels.size(); i++){
                wheels[i].stopWinningAnimation();
            }
            
            
            for(int i = 0; i < wheels.size(); i++){
                wheels[i].moveToFruit((int)(e.key - '0'));
            }
    }
    
    
    if (e.key == OF_KEY_UP){
       int target= wheels[0].getFruitTarget();
        int newTarget=target+1;
        if (newTarget>totalNumberOfImages) {
            newTarget=0;
        }
        
        for(int i = 0; i < wheels.size(); i++){
            
            
            wheels[i].moveToFruit(target+1);
        }

    }
    
    if(e.key == 'p'){
   // string myname=generateTicket();
   // string sysCommand="lp -d AVERYNT -o media=Custom.101x158mm -o scale-to-page ../../../data/"+myname;
   //  const char* valChar = (const char*) sysCommand.c_str();
     //   system (valChar);
    }
    

    switch (e.key) {

            
        case 'd':
            
            castDice();
            
           // wheels[0].speed((int)(ofRandom(0, 40)));
           // wheels[1].moveToFruit((int)(ofRandom(0, 4)));
            
            
            break;
       
        case 't':
            
            udp_manager->sendModeToId(1,TURNING);
            
            // wheels[0].speed((int)(ofRandom(0, 40)));
            // wheels[1].moveToFruit((int)(ofRandom(0, 4)));
            
            
            break;

            
            case 'z':
            // illegal access
            *p = 0xAB;
            
            break;
            
            
            
        case 's':
            cout<<"send"<<endl;
          //  udp_manager->sendDebug();
            udp_manager->sendModeToId(1,STOP);

            
            // wheels[0].speed((int)(ofRandom(0, 40)));
            // wheels[1].moveToFruit((int)(ofRandom(0, 4)));
            
            for(int i = 0; i < wheels.size(); i++){
                wheels[i].stopWinningAnimation();
            }
            
            
            break;
            
            
           case 'w':
            //winmanager->checkWin();
            
            startWinningAnimation();
            
            udp_manager->sendModeToId(1,200);
            udp_manager->sendModeToId(2,200);

            
            break;
            
        case 'P':
            winmanager->makeDalyPrices();
          //  winmanager->checkIfWin();
            break;
            
            
        case 'p':
            myPrice=winmanager->checkIfWin();
            cout<<myPrice.sort<<endl;
            
            break;

    
            
        case 'm':
            for(int i = 0; i < wheels.size(); i++){
                float r=ofRandom(50,80);
                wheels[i].startRolling(r);
            }
            break;
            
        case 'r':
            for(int i = 0; i < wheels.size(); i++){
                wheels[i].reset();
            }
            break;
            
            
            
    }
}


void Slotmachinecontroller::castDice(){
   udp_manager->sendModeToId(1,TURNING);

    for(int i = 0; i < wheels.size(); i++){
        wheels[i].stopWinningAnimation();
    }

    
    int rollduration=ofRandom(2000, 3000);
    int wheelldiff=ofRandom(2000, 3000);

   // int rollduration=ofRandom(100, 1000);
   // int wheelldiff=ofRandom(100, 1000);


    
    
    myPrice=winmanager->checkIfWin();
    cout<<" mysort "<<myPrice.sort<<endl;
    if(myPrice.sort!="loose"){
        bIsWinning=true;
        int rand=ofRandom(0,totalNumberOfImages);
        string myname=generateTicket(rand, myPrice.sort);
        actualName=myname;
        
        cout<<"YOU WIN! "<<rand<<" name "<<actualName<<" sort "<<myPrice.sort<<endl;

        for(int i = 0; i < wheels.size(); i++){
            wheels[i].moveToFruitWithTimeOffsett(rand, rollduration+i*wheelldiff);
        }

        
    
    }else{
    
        
        bIsWinning=false;
        
        int oldr;
        int rand=ofRandom(0,totalNumberOfImages);
        
        int almostWin=ofRandom(0,4);
        
        if(almostWin<4){
            rand=ofRandom(0,totalNumberOfImages);
            
            for(int i = 0; i < wheels.size(); i++){
                if(i<almostWin){
                wheels[i].moveToFruitWithTimeOffsett(rand, rollduration+i*wheelldiff);
                }else{
               
                    int lastrand=rand;
                    do
                    {
                       lastrand=ofRandom(0,totalNumberOfImages);
                        if(lastrand==rand)lastrand=ofRandom(0,totalNumberOfImages);
                        if(lastrand==oldr)lastrand=ofRandom(0,totalNumberOfImages);
                        cout<<rand<< " lastrand "<<lastrand<<endl;
                    } while (lastrand==rand && oldr==lastrand);
                    oldr=lastrand;
                    
                    
                    wheels[i].moveToFruitWithTimeOffsett(lastrand, rollduration+i*wheelldiff);
                }
            }
            
        }/*else{
        
        for(int i = 0; i < wheels.size(); i++){
            do
            {
                rand=ofRandom(0,totalNumberOfImages);
                cout<<"rand"<<rand<<endl;
            } while (oldr==rand);

            cout<<"YOU Loose! "<<rand<<endl;
            wheels[i].moveToFruitWithTimeOffsett(rand, rollduration+i*wheelldiff);
            oldr=rand;
        }
        }*/

    
    }
    
  
    
}



void Slotmachinecontroller::wheelStopped(int &i){
    cout<<"Stopped: "<<i<<endl;
    
    
    if(i==wheels.size() -1&& bIsWinning){
        cout<<"--------YAYYY---"<<i<<endl;
        startWinningAnimation();
          udp_manager->sendModeToId(1, STOP);
          udp_manager->sendWin();
        
        //start Winning LED's
      
    }
    
    
    
    if(i==wheels.size() -1 && !bIsWinning){
        udp_manager->sendModeToId(1, STOP);
    }
    
    
    
    
    
    if(i==wheels.size() -3&& bIsWinning){
        cout<<"----start printing job---"<<i<<endl;
        string sysCommand="lp -d AVERYNT -o media=Custom.101x200mm -o scale-to-page ../../../data/"+actualName;
        const char* valChar = (const char*) sysCommand.c_str();
        system (valChar);
        
    }


}



void Slotmachinecontroller::startWinningAnimation(){
    
    int offsetTime=0;
    int speed=100;
    int animationPause=100;
    int wheeloffset=200;
    
    
    for(int i = 0; i < wheels.size(); i++){
       int target= wheels[i].getFruitTarget();
        ofColor c=wheels[i].getTargetFruitSecondColor();
       wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        offsetTime+=speed;
    }
    offsetTime+=animationPause;

    int counter=0;
    for(int i = wheels.size()-1; i>=0; i--){
        ofColor c=wheels[i].getTargetFruitColor();
        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        counter++;
        offsetTime+=speed;
    }
   
    offsetTime+=animationPause;

    for(int i = 0; i < wheels.size(); i++){
        ofColor c=wheels[i].getTargetFruitSecondColor();
        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        offsetTime+=speed;
    }
    offsetTime+=animationPause;
     counter=0;
    for(int i = wheels.size()-1; i>=0; i--){
        ofColor c=wheels[i].getTargetFruitColor();
        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        offsetTime+=speed;
        counter++;
    }
    offsetTime+=animationPause;
    
    speed=200;
    
    for (int i=0;i<10;i++){
            if(i%2==0){
                for(int k = 0; k < wheels.size(); k++){
                    wheels[k].startWinningAnimationWithTimeOffset(offsetTime,ofColor(255),speed);
                }
               // offsetTime+=animationPause;


            }else{
                for(int k = 0; k < wheels.size(); k++){
                    ofColor c=wheels[k].getTargetFruitSecondColor();

                    wheels[k].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
                }
                //offsetTime+=animationPause;


            }
        offsetTime+=300;

    }
    
    offsetTime+=animationPause;

    
    speed=100;
    for(int i = 0; i < wheels.size(); i++){
        ofColor c=wheels[i].getTargetFruitColor();
        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        offsetTime+=speed;
    }
    offsetTime+=animationPause;
    
     counter=0;
    for(int i = wheels.size()-1; i>=0; i--){
        ofColor c=wheels[i].getTargetFruitSecondColor();
        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        counter++;
        offsetTime+=speed;
    }
    
    offsetTime+=animationPause;
    
    for(int i = 0; i < wheels.size(); i++){
        ofColor c=wheels[i].getTargetFruitColor();

        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        offsetTime+=speed;
    }
    offsetTime+=animationPause;
    counter=0;
    for(int i = wheels.size()-1; i>=0; i--){
        ofColor c=wheels[i].getTargetFruitSecondColor();

        wheels[i].startWinningAnimationWithTimeOffset(offsetTime,c,speed);
        offsetTime+=speed;
        counter++;
    }
    offsetTime+=animationPause;
}


void Slotmachinecontroller::stopWinningAnimation(){
    
    
    
    
}



//--------------------------------------------------------------
string Slotmachinecontroller::generateTicket(int _tnumber, string _sort){
    
   
    
   
    
    string ticketname="tickets/ticket-"+ofToString(ofGetUnixTime())+"_"+ofToString(ofGetLastFrameTime())+".jpg";

    //fbo is the main drawing canvas displayed in the second window
    cout<<ticketname<<" sort "<<_sort<<endl;
    
    
    
    myImage.loadImage("ticket_vorlage/ticket_"+_sort+".png");

    
    int w = myImage.width;
    int h = myImage.height;
    
    
    
    unsigned char* pixels = new unsigned char[w*h*3];
    
    //ofImage screenGrab;
    mergedImage.setUseTexture(false);
    
    generatedTicket.begin();
    ofClear(255,255,255,0);
    ofSetColor(255, 255, 255);
    myImage.draw(0,0);
    
    if(_sort=="super"){
    ofPushMatrix();
    ofTranslate(myImage.width/2, 0);
    ofScale(.8,.8);
    ofFill();
    ofEnableAlphaBlending();
    icons_sw[_tnumber].draw(-icons_sw[_tnumber].width/2,520);
    ofDisableAlphaBlending();
    ofPopMatrix();
    
    
    

        ofPushMatrix();
        
        ofSetColor(0);
        ofTranslate(250, myImage.height-180);
        //ofScale(2,2);
        ofFill();
        ofEnableAlphaBlending();
        string number=ofToString(int(ofRandom(10000,90000)));
        string number2=ofToString(int(ofRandom(100,300)));

        
       string m=ofToString(ofGetMonth());
        string d=ofToString(ofGetDay());

        mono.drawString(random_string(2)+number+"#"+d+m+random_string(5)+number2, 0, 0);
        cout<<(random_string(2)+number+"#"+d+m+random_string(5)+number2);

        
        ofDisableAlphaBlending();
        
        ofPopMatrix();

    
    
    }
    else{
        
        ofPushMatrix();
        ofTranslate(myImage.width/2, 0);
       // ofScale(2.5,2.5);
        ofFill();
        ofEnableAlphaBlending();
        icons_sw[_tnumber].draw(-icons_sw[_tnumber].width/2,500);
        ofDisableAlphaBlending();
        ofPopMatrix();
        
        
        ofPushMatrix();
        
        ofSetColor(0);
        ofTranslate(250, myImage.height-180);
        ofFill();
        ofEnableAlphaBlending();
        string number=ofToString(int(ofRandom(10000,90000)));
        string number2=ofToString(int(ofRandom(100,300)));
        
        string m=ofToString(ofGetMonth());
        string d=ofToString(ofGetDay());
        
        mono.drawString(random_string(2)+number+random_string(8)+number2, 0, 0);
        ofDisableAlphaBlending();
        ofPopMatrix();
    
    
    }
    
    
    
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    
    glReadPixels(0, 0, generatedTicket.getWidth(), generatedTicket.getHeight(), GL_RGB, GL_UNSIGNED_BYTE, pixels);
    mergedImage.setFromPixels(pixels, generatedTicket.getWidth(), generatedTicket.getHeight(), OF_IMAGE_COLOR);
    
    mergedImage.saveImage(ticketname);
    
    

    
    
    
    generatedTicket.end();

    
    return ticketname;
    
    
}




void Slotmachinecontroller::setWinningThreshold(float threshold){
    winningThreshold=threshold;
    
}
float Slotmachinecontroller::getWinningThreshold(){
    return winningThreshold;
}



void Slotmachinecontroller::addKeyListener(){
    ofAddListener(ofEvents().keyPressed, this, &Slotmachinecontroller::keyPressed);
    bAddedKeyListener=true;
}


void Slotmachinecontroller::removeKeyListener(){
    ofRemoveListener(ofEvents().keyPressed, this, &Slotmachinecontroller::keyPressed);
    bAddedKeyListener=false;
}

bool Slotmachinecontroller::isKeyListening() {
    return bAddedKeyListener;
}

void Slotmachinecontroller:: turnOnKeyListener(){
    if(!isKeyListening()){
        addKeyListener();
    }
}

void Slotmachinecontroller::turnOffKeyListener(){
    if(isKeyListening()){
        removeKeyListener();
    }
}


void Slotmachinecontroller::minSliderChanged(int & minSpeedSlider){
    minSpeed=minSpeedSlider;
    for(int i = 0; i < 4; i++){
       wheels[i].setSpeed(minSpeed,maxSpeed);
    }
    gui.saveToFile("GUIsettings.xml");

}

void Slotmachinecontroller::maxSliderChanged(int & maxSpeedSlider){
    maxSpeed=maxSpeedSlider;
    for(int i = 0; i < 4; i++){
      wheels[i].setSpeed(minSpeed,maxSpeed);
    }
    gui.saveToFile("GUIsettings.xml");

}



string Slotmachinecontroller:: random_string( int length )
{
    string s="";
    static const char alphanum[] =
    //"0123456789"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //"abcdefghijklmnopqrstuvwxyz";
    
    for (int i = 0; i < length; ++i) {
        s+= alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return s;
}