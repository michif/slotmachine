//
//  winmanager.h
//  slotmachine
//
//  Created by Michael Flueckiger on 08.06.15.
//
//

#ifndef __slotmachine__winmanager__
#define __slotmachine__winmanager__

#include <stdio.h>
#include "ofMain.h"
#include "ofxXmlSettings.h"





struct pricesStruct {
    
    string title;
    string number;
    string sort;
    int wintime;
    bool isGone;
    int myid;
    
    
};


class WinManager {
    
public:

    WinManager();
    virtual ~WinManager();
    
    ofxXmlSettings XML;

    ofxXmlSettings settings;

    int starttime;
    int endtime;
    
    
    void setup();
    void loadWinlist();
    
    void checkWin();
    
    
    pricesStruct checkIfWin();

    
    vector<string>win;

    vector<pricesStruct>dalyprices;
    
void makeDalyPrices();
    
    

};

#endif /* defined(__slotmachine__winmanager__) */
