//
//  wheel.h
//  slotmachine
//
//  Created by Michael Flueckiger on 12.05.15.
//
//

#ifndef __slotmachine__wheel__
#define __slotmachine__wheel__

#include <stdio.h>


#include "ofMain.h"
#include "movingObject.h"



struct Timercomponent {
    int   millis;
    int startAnimationTime;
    bool bTimerReached;
    ofColor color;
    float speed;
};


class Wheel {

    
    
public:
    /*
    Wheel ();
    ~Wheel();*/
    
    // OF  Functions
    void setup();
    void update();
    void draw();
    void clear();

    
    int myId;
    void setId(int _id);
    int getId();
    
    vector<ofImage>icons;
    float iconHeight;
    
    
       ofImage shadow;
    ofImage trenner;
    
    float wheellength;
    
    
    int maxFruits;
    vector<MovingObject>fruits;

    

    void setPosition(ofVec3f position);
    ofVec2f getPosition();
    
    
    void startWinningAnimationWithTimeOffset(int milis, ofColor col, float speed=200);
    float startAnimationTime;
    void startWinningAnimation();
    void stopWinningAnimation();
    void winningAnimation();
    
    vector<Timercomponent>timervec;

    
    
    
    bool bAnimationTimerReached;
    

    
    
    float speed;
    
    
    ofVec2f moveToFruit(int fruitNumber);
    ofVec2f moveToFruitWithTimeOffsett(int fruitNumber, float _milliseconds);

    
    
    void reset();
    
    void startRolling(float speed);
    void setSpeed(int _minSpeed, int _maxSpeed);
    
    
    float getSpeed();

    
    void makeARound();
    void setAllStops();

    
    float stopPos;
    float breakPoint;
    float scalefact;
    
    
    float startTime; // store when we start time timer
    float endTime; // when do want to stop the timer
    
    bool  bTimerReached; // used as a trigger when we hit the timer
    
    
    void setFruitTarget(int _fruitTarget);
    int getFruitTarget();

    
    
    ofTrueTypeFont  franklinBook14;
    string typeStr;

    ofColor getTargetFruitColor();
    ofColor getTargetFruitSecondColor();
    
    
    
    ofEvent<int> hasStopped;
    
    
    void newEvent(int & i);

    int minSpeed;
    int maxSpeed;
    
    
private:
    ofVec2f position;
    int fruitTarget;
    
    float spinningSpeed;
    

};

#endif /* defined(__slotmachine__wheel__) */
