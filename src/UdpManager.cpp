//
//  UdpManager.cpp
//  ColorWindows
//
//  Created by Michael Flueckiger on 24.09.12.
//  Copyright (c) 2012 Ich. All rights reserved.
//

#include <iostream>
#include "UdpManger.h"

//
//  SerialManager.cpp
//  ColorWindows
//
//  Created by Michael Flueckiger on 11.09.12.
//  Copyright (c) 2012 Ich. All rights reserved.
//

#include <iostream>
//#include "SerialManager.h"
#include "slotmachinecontroller.h"



UdpManager::UdpManager(){
    init();
}


UdpManager::~UdpManager(){
    
}


//------------------------------------------------------


void UdpManager::init(){
     
    cout<<"init"<<endl;
    /*
    for(int i=0;i<NUM_CLIENTS;i++){
        //create the socket and set to send to 127.0.0.1:11999
        
        udpConnection[i].Create();        
        
        string ipstring="192.168.1.";
        ipstring+=ofToString(i);d
        
        
         char* valChar = ( char*) ipstring.c_str(); // cast from string to unsigned char*
        cout<<ipstring<<endl;

        
        udpConnection[i].Connect(valChar,8888);
        cout<<valChar<<endl;
        udpConnection[i].SetNonBlocking(true);
        
    }*/
    
    //udpConnection[0].Create();
    //create the socket and set to send to 127.0.0.1:11999
    //udpConnection[0].Connect();
         // udpConnection[0].Bind(8888);
   // udpConnection[0].BindMcast("192.168.1.100", 8888);
    //udpConnection[0].SetNonBlocking(true);
    
    udpConnection[0].Create();
    udpConnection[0].Connect("192.168.1.100",11999);
    udpConnection[0].Bind(8888);
    udpConnection[0].SetNonBlocking(true);
    

    udpConnection[1].Create();
    udpConnection[1].Connect("192.168.1.177",8888);
    udpConnection[1].Bind(11999);
    udpConnection[1].SetNonBlocking(true);
    
    
    udpConnection[2].Create();
    udpConnection[2].Connect("192.168.1.178",8888);
    udpConnection[2].Bind(11999);
    udpConnection[2].SetNonBlocking(true);


    
    
}


void UdpManager::update(){
    //
   // cout<<"SerialManager is updating"<<endl;
    //__color buffer______________________
    // updateColorBuffer();
    
    for(int i=0;i<NUM_CLIENTS;i++){
       // APPC->setIsInFront(i,false);
        char udpMessage[100000];
        udpConnection[i].Receive(udpMessage,100000);
        string message=udpMessage;
        if(message!=""){
            // bool b=toBool(message);
            cout<<"message: "<< message<< " from client "<<i<<endl;

            if(message=="acknowledged"){
                APPC->castDice();
                //APPC->setIsInFront(i,true);
                //cout<<"message: "<< message<< " from clinet "<<i<<endl;
                //APPC->windows[i].setIsInfront(true);
            }
            if(message=="0"){
                //APPC//->setIsInFront(i,false);
            }
        }    
    }
  //  updateUdpSendData();
}


bool UdpManager::toBool(std::string const& s) {
    return s != "0";
}


void UdpManager::draw(){
    //  cout<<"SerialManager is Drawing"<<endl;
}


void UdpManager::exit(){
    // cout<<"exit SerialManager"<<endl;
}




//EVENTS
//------------------------------------------------------

void UdpManager::addListeners() {
    if(!bAddedListeners){
        ofAddListener(ofEvents().update, this, &UdpManager::_update);
        ofAddListener(ofEvents().draw, this, &UdpManager::_draw);
        ofAddListener(ofEvents().exit, this, &UdpManager::_exit);
        
    }
    bAddedListeners = true;
}


void UdpManager::removeListeners() {
	ofRemoveListener(ofEvents().update,this,&UdpManager::_update);
    ofRemoveListener(ofEvents().draw,this,&UdpManager::_draw);
    ofRemoveListener(ofEvents().exit,this,&UdpManager::_exit);
    
    bAddedListeners = false;
}


//EVENT HANDLER
//------------------------------------------------------

void UdpManager :: _update( ofEventArgs &e )
{
	update();
}

void UdpManager :: _draw( ofEventArgs &e )
{
	draw();
}


void UdpManager :: _exit( ofEventArgs &e )
{
	exit();
}


void UdpManager::sendDebug(){
    
    string message="Hello";
    //  message+=r+","+g+","+b+","+speed+","+mode+",0";
    int sent = udpConnection[1].Send(message.c_str(),message.length());
    
}


void UdpManager::sendToId(int _id, ofColor _col, float _speed, int _mode){

    int sendId=_id;
    
    string r,g,b,speed,mode;
    
    //int br=_col.getBrightness();
   // _col.setBrightness(br/2);
    
    int temp;
    temp=_col.r;
    if (temp==44) temp=45;   
    r=ofToString(floor(ofMap(temp, 0, 255, 0, 127)));
    temp=_col.g;
    if (temp==44) temp=45;   
    g=ofToString(floor(ofMap(temp, 0, 255, 0, 127)));
    temp=_col.b;
    if (temp==44) temp=45;   
    b=ofToString(floor(ofMap(temp, 0, 255, 0, 127)));
    if(_id==0){
    }
    temp=_speed;
    if (temp==44) temp=45;   
    
    speed =ofToString(temp);
    mode=ofToString(_mode);
    string message="";
    message+=r+","+g+","+b+","+speed+","+mode+",0";
    int sent = udpConnection[sendId].Send(message.c_str(),message.length());
  //  cout<<"was sent to "<<sendId<<endl;

}



void UdpManager::sendWinToId(int _id, int _mode){
    int sendId=_id;
    
    string mode=ofToString(_mode);

    
    string message="";
    message+=mode;
    message+=",0";
    int sent = udpConnection[sendId].Send(message.c_str(),message.length());
    

}




void UdpManager::sendModeToId(int _id, int _mode){
    int sendId=_id;
    string mode=ofToString(_mode);
    
    string message="";
    message+=mode;
    message+=",0";
    int sent = udpConnection[sendId].Send(message.c_str(),message.length());
    
    
}




void UdpManager::sendWin(){
    int sendId=0;
    
    string mode=ofToString(200);
    
    
    string message="";
    message+=mode;
    message+=",0";
    int sent = udpConnection[1].Send(message.c_str(),message.length());
     sent = udpConnection[2].Send(message.c_str(),message.length());

}


void UdpManager::sendBlack(){
    int sendId=3;
    
    string mode="RTE 5";
    
    string message="";
    message+=mode;
    message+="\r";
    int sent = udpConnection[sendId].Send(message.c_str(),message.length());
    
}




//--------------------------------------------------------------

void UdpManager::updateUdpSendData(){
    
    for(int i=0;i<NUM_CLIENTS;i++){
        /*
        if(APPC->windows[i].hasUpdatedColor()){
            ofColor sendCol= APPC->windows[i].getColor();
            sendToId(i,sendCol,0,SET_COLOR);
            APPC->windows[i].hasReadUpdateBuffer();
            

            if(i==0){
           // cout<<"window "<<i<<" has updated color "<< sendCol<<endl;
            }
            
           
        }*/
    }
}



//--------------------------------------------------------------

void UdpManager::updateColorBuffer(){
    
    writeToArduino=false;
    if(ofGetFrameNum()%2==0)writeToArduino=true;
    
    if(writeToArduino){
       /* for(int i=0;i<windowNum;i++){
            if(APPC->windows[i].hasUpdatedColor()){
                APPC->windows[i].hasReadUpdateBuffer();
                ofColor sendCol= APPC->windows[i].getColor();
                colorbuffer.push_back(i);
                
                if(sendCol.r==26)sendCol.r=27;
                if(sendCol.g==26)sendCol.g=27;
                if(sendCol.b==26)sendCol.b=27;
                
                colorbuffer.push_back(sendCol.r);
                colorbuffer.push_back(sendCol.g);
                colorbuffer.push_back(sendCol.b);
                colorbuffer.push_back(26);
            }
        }*/
    }
    
    
    int shiftAmmount=10;
    if(colorbuffer.size()>=shiftAmmount){  
        for (int i=0;i<shiftAmmount;i++){
            serial.writeByte(colorbuffer[i]);
        }
        colorbuffer.erase(colorbuffer.begin(),colorbuffer.begin()+shiftAmmount);
    }
    else{
        int size=colorbuffer.size();
        for (int i=0;i<size;i++){
            serial.writeByte(colorbuffer[i]);
        }      
        colorbuffer.erase(colorbuffer.begin(),colorbuffer.begin()+size);
    }   
    if(colorbuffer.size()>0){
        //  cout<<"colorBuffer size"<<colorbuffer.size()<<endl;
    }
}












//Event Helper
//------------------------------------------------------

void UdpManager::turnOn(){
    if(!bAddedListeners){
        addListeners();
    }
}


void UdpManager::turnOff(){
    if(bAddedListeners){
        removeListeners();
    }
}


bool UdpManager::isRunning(){
    return bAddedListeners;
}


void UdpManager::reset(){
    //reset App
}

void UdpManager::setWindowNum(int _num){
    windowNum=_num;
}

