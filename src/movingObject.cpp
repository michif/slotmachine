//
//  movingObject.cpp
//  Tangodance
//
//  Created by Michael Flueckiger on 14.09.14.
//
//

#include "movingObject.h"

// the static event, or any static variable, must be initialized outside of the class definition.
ofEvent<int> MovingObject::targetObjectStopped = ofEvent<int>();



MovingObject :: MovingObject ()
{
    cout << "creating MovingObject" << endl;
     speed.set(0,0,0);

}

MovingObject :: ~MovingObject ()
{
    cout << "destroying MovingObject" << endl;
    clear();
    
}

//--------------------------------------------------------------
void MovingObject::setup(){

    drag=0.85;
    position.set(0,0,0);
    maxSpeed=1;
    bIsMoving=false;
    target.set(0,200,0);
    bIsSeeking=false;
    bAccelerated=true;
    breakpoint=ofVec3f(0,0,0);
    
    rasterimg.loadImage("raster_weiss.png");
    
    mycol=ofColor(ofRandom(100,255),ofRandom(100,255),ofRandom(100,255));
    
    swatch.setColor(ofColor(mycol));
    
    iconHeight=240;
    
    //startMove();
   //setSpeed(ofVec3f(0,50,0));
    
}


//--------------------------------------------------------------
void MovingObject::update(){
    move();
    swatch.update();


}


//--------------------------------------------------------------
void MovingObject::draw(){
    ofPushMatrix();
    
    ofTranslate(position.x,position.y);
    //ofRect(0, 0, 100, 100);
    //ofEnableAlphaBlending();
    
   //if(bRecalcTarget)
    ofRect(0, 0,iconHeight,iconHeight);
    
    if(position.y>-iconHeight &&  position.y<iconHeight){
        float pos=abs(position.y);
           // ofSetColor(swatch.getSetColor(),ofMap(pos, 0, image.height, 255, 0));
    }else{
       // ofSetColor(255,255,255);
       
    }
    
    if(bIsStopingIcon && speed.length()<20){
       ofSetColor(swatch.getActualColor());
    }
  //  ofSetColor(swatch.getSetColor());

    
    
    rasterimg.draw(0,0,0,iconHeight,iconHeight);
    

    ofSetColor(255,255,255);
    image.draw(0,0,0,iconHeight,iconHeight);
    

  
    //ofDisableAlphaBlending();

    

    
    ofPopMatrix();
    
  
    
}


void MovingObject::startMove(){
    bIsMoving=true;
    bIsSeeking=false;
    //swatch.setFadeColor(ofColor(ofRandom(100,255),ofRandom(100,255),ofRandom(100,255)));
    swatch.setColor(ofColor(255));


};
void MovingObject::endMove(){
    bIsMoving=false;
    bIsSeeking=false;
    position.set(target);
    speed.set(0,0,0);
    bIsMoving=false;
    bIsSeeking=false;
    //swatch.setFadeColor(ofColor(255,0,0));
    if(bIsStopingIcon){
        //swatch.setFadeColor(mycol,0.2);
        
        setFadeTo(mycol,200);
            ofNotifyEvent(targetObjectStopped, myWheelId);
    }


};
void MovingObject::moveTo(ofVec3f(_target)){
    
    setTarget(_target);
    bIsSeeking=true;
    bIsMoving=true;
};

//--------------------------------------------------------------
void MovingObject::move(){

    if(bIsMoving){
        if(bIsSeeking){
            ofVec3f accel;
            ofVec3f distance;
            
            if(bAccelerated){
                distance.set(target);
                distance-=(position);
              //  distance.limit(maxSpeed);
                accel.set(distance);
                
                speed+=(accel);
                speed.limit(maxSpeed);
                speed*=(drag);
                
                position+=speed;
                if(distance.length()<10 && speed.length()<10){
                    endMove();
                }
            }
            
            else{
                distance.set(target);
                distance-=(position);
                speed.set(distance);
                speed.limit(maxSpeed);
                position+=speed;
                if(distance.length()<0.0001 && speed.length()<0.0001){
                  
                    endMove();

                }
            }
        }else{
            position+=speed;
        }
    }
}


void MovingObject::clear(){

}



//--------------------------------------------------------------

void MovingObject::setImage(string _path){
    image.loadImage(_path);
    cout<<"load "<<_path<<endl;
};




//--------------------------------------------------------------

void MovingObject::setTarget(ofVec3f _newTarget){
    
//    if(_newTarget.y>ofGetHeight())_newTarget.y=ofGetHeight()-_newTarget.y;
    target.set(_newTarget);
    
    
};
//--------------------------------------------------------------

ofVec3f MovingObject::getTarget(){
    return target;
};


//--------------------------------------------------------------

void MovingObject::setTargetOffset(ofVec3f _newTarget){
    targetOffset.set(_newTarget);
};

ofVec3f MovingObject::getTargetOffset(){
    return targetOffset;
};

//--------------------------------------------------------------

void MovingObject::setPosition(ofVec3f(newposition)){
    position.set(newposition);
};
//--------------------------------------------------------------

ofVec3f MovingObject::getPosition(){
    return position;
};

void  MovingObject::setStopPos(ofVec3f s){
    stopPos.set(s);
}

ofVec3f  MovingObject::getStopPos(){
    return stopPos;
}
void  MovingObject::setBreakPoint(ofVec3f s){
    breakpoint.set(s);
}

ofVec3f  MovingObject::getBreakPoint(){
    return breakpoint;
}


//--------------------------------------------------------------

//--------------------------------------------------------------
void MovingObject::setSpeed(ofVec3f s){
    speed.set(s);    
}

ofVec3f MovingObject::getSpeed(){
    return speed;

}

void MovingObject::setMaxSpeed(float _maxSpeed){
    maxSpeed=_maxSpeed;
};
//--------------------------------------------------------------

float MovingObject::getMaxSpeed(){
    return maxSpeed;
};




//--------------------------------------------------------------

void MovingObject::setAccelerated(bool acc){
    bAccelerated=acc;
};
//--------------------------------------------------------------

bool MovingObject::getAccelerated(){
    return bAccelerated;
};

//--------------------------------------------------------------

void MovingObject::madeARound(){
    if(bRecalcTarget){
        recalcTarget();
    }
};

void MovingObject::recalcTarget(){
    
    ofVec3f t=ofVec3f(normalPosition);
    t+=targetOffset;
    

    
    if(t.y>=breakpoint.y){
        t.y=-wheellength+t.y;
    }
    
    
    setTarget(t);

    
    bRecalcTarget=false;
};


void MovingObject::setRecalcTarget(bool _bRecalcTarget){
    bRecalcTarget=_bRecalcTarget;
};


//--------------------------------------------------------------

void MovingObject::setNormalPosition(ofVec3f(position)){
    normalPosition.set(position);
};
//--------------------------------------------------------------

ofVec3f MovingObject::getNormalPosition(){
    return normalPosition;
};


void MovingObject::setWheellength(float h){
    wheellength=h;
};


void MovingObject::setIsStopingIcon(bool _bisStopingicon){
    bIsStopingIcon=_bisStopingicon;
}

bool MovingObject::getIsStopingIcon(){
    return bIsStopingIcon;
}



void MovingObject::setSecondColor(ofColor _col){
    mySecondColor=_col;
    swatch.setSecondColor(ofColor(mycol));    
}

ofColor MovingObject::getSecondColor(){
    return mySecondColor;
}


void MovingObject::setColor(ofColor _col){
    mycol=_col;
    swatch.setColor(ofColor(mycol));

}

ofColor MovingObject::getColor(){
    return mycol;
}


void MovingObject::setId( int _id){
    myId=_id;
}

int MovingObject::getId(){
    return myId;
}


void MovingObject::setWheelId( int _id){
    myWheelId=_id;
}

int MovingObject::getWheelId(){
    return myWheelId;
}




void MovingObject::setFadeTo(ofColor col, float millis){
    
    swatch.setFadeColor(col,millis);


}


