//
//  movingObject.h
//  Tangodance
//
//  Created by Michael Flueckiger on 14.09.14.
//
//

#ifndef __slotmachine__movingObject__
#define __slotmachine__movingObject__

#include <iostream>
#include "ofMain.h"
#include "Colorswatch.h"
#include "ofxGui.h"

#include "ofEvents.h"




class MovingObject {
    
    
	
    public:
    
    MovingObject ();
    ~MovingObject ();

    void setup();
    void update();
    void draw();
    void clear();

    
    void setImage(string filepath);
    ofImage image;

    ofImage rasterimg;

    int iconHeight;
    
    
    int myId;
    void setId(int _id);
    int getId();
    
    int myWheelId;
    void setWheelId(int _id);
    int getWheelId();
    
    
    void startMove();
    void endMove();
    void moveTo(ofVec3f(_target));
    
    
    void setTarget(ofVec3f(newTarget));
    ofVec3f getTarget();
    
    
    void setTargetOffset(ofVec3f(newTarget));
    ofVec3f getTargetOffset();
    
    
    void setPosition(ofVec3f(newposition));
    ofVec3f getPosition();
    
    
    void setNormalPosition(ofVec3f(position));
    ofVec3f getNormalPosition();
    
    void setMaxSpeed(float maxSpeed);
    float getMaxSpeed();
    
    void setAccelerated(bool acc);
    bool getAccelerated();
    
    void setSpeed(ofVec3f speed);
    
    void setStopPos(ofVec3f stopPos);
    ofVec3f getStopPos();
    
    void setBreakPoint(ofVec3f breakPoint);
    ofVec3f getBreakPoint();
    
    void madeARound();
    void recalcTarget();

    void setRecalcTarget(bool _bRecalcTarget);
    

    void setWheellength(float height);
    ofVec3f stopOffset;

    ofColor mycol;

    void setIsStopingIcon(bool _bisStopingicon);
    bool getIsStopingIcon();
    
    
    
    void setFadeTo(ofColor col, float seconds);
    
   Colorswatch swatch;

    ofVec3f getSpeed();
    
    
    void setColor(ofColor _col);
    ofColor getColor();
    
    
    void setSecondColor(ofColor _col);
    ofColor getSecondColor();
    
    
    void setBlinkColor(ofColor _col);
    ofColor getBlinkColor();
    
    void setBlinking(float blinking, float waiting);
    void startBlinking();
    void stopBlinking();
    
    static ofEvent<int> targetObjectStopped;
    
    
    
    
private:
    bool bRecalcTarget;
    
    bool bIsStopingIcon;
    
    ofVec3f breakpoint;
    ofVec3f stopPos;
    ofVec3f targetOffset;
    
    void move();
    
    float drag;
    bool bIsMoving;
    bool bIsSeeking;
    
    ofVec3f position;
    ofVec3f normalPosition;
    
    ofVec3f target;
    bool bAccelerated=false;
    float maxSpeed;
    ofVec3f speed;
    
    
    float wheellength;
    
    bool bIsBlinking;

    
    ofColor mySecondColor;


    
    
};

#endif /* defined(__slotmachinee__movingObject__) */
