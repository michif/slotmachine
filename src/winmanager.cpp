//
//  winmanager.cpp
//  slotmachine
//
//  Created by Michael Flueckiger on 08.06.15.
//
//
#include <iostream>

#include "winmanager.h"


WinManager::WinManager(){
}


WinManager::~WinManager(){
    
}



//--------------------------------------------------------------
void WinManager::setup(){
    cout<<"winmanager setup"<<endl;
    
    
    if( settings.load("settings.xml") ){
        
        starttime=settings.getValue("settings:starttime", 8);
        endtime=settings.getValue("settings:endtime", 22);
        
        settings.saveFile("settings.xml"); //puts settings.xml file in the bin/data folder

        
    }else{
        //put some some settings into a file
        settings.setValue("settings:speed", 40);
        settings.setValue("settings:totalminutes", 840);
        settings.setValue("settings:starttime", 8);
        settings.setValue("settings:endtime", 22);
        
        starttime=8;
        endtime=22;


        settings.setValue("prices:normal", 15);
        settings.setValue("prices:special", 9);
        settings.setValue("prices:super", 1);
        settings.saveFile("settings.xml"); //puts settings.xml file in the bin/data folder
        
        


          }

    
    
   

   /* int day=ofGetDay();
    string daystring;
    daystring+=ofToString(day,2);
    
    int tagNum = settings.addTag(daystring);
    settings.setValue(daystring+":prices:super", 2, tagNum);

*/
    
     makeDalyPrices();
    
}



pricesStruct WinManager::checkIfWin(){
    /*
    if(dalyprices.size()<1){
        makeDalyPrices();
    }*/
    
    pricesStruct myPrice;
    myPrice.sort="loose";
    
    

    int actualMinute;
    int h=ofGetHours();
    int m=ofGetMinutes();
    
    
    int minsfromhours=(h-starttime)*60;
    actualMinute=minsfromhours+m;
    
    
    
    
    for (int i =0;i<dalyprices.size();i++){
        cout<<"myMinute "<<actualMinute<<" dalyprices[i].wintime "<<dalyprices[i].wintime<<" sort "<<dalyprices[i].sort<<endl;

        if(dalyprices[i].wintime<=actualMinute && dalyprices[i].isGone==false ){
            cout<<"you win! "<<dalyprices[i].myid <<" is my id"<<endl;
            dalyprices[i].isGone=true;
            
            myPrice.title=dalyprices[i].title;
            myPrice.number=dalyprices[i].number;
            myPrice.sort=dalyprices[i].sort;
            myPrice.wintime=dalyprices[i].wintime;
            myPrice.isGone=dalyprices[i].isGone;
            myPrice.myid=dalyprices[i].myid;


            //SAVE WIN IN XML
            int day=ofGetDay();
            int month=ofGetMonth();
            
            string daystring;
            daystring+=ofToString(day,2);
            
            string monthstring="WintimesForDay_";
            monthstring+=ofToString(month,2);
            
            string dateString=monthstring+"_"+daystring;
            int numdayTags = settings.getNumTags(dateString);
            cout<<numdayTags<<endl;
            
            settings.pushTag(dateString, numdayTags-1);
                settings.pushTag("price", dalyprices[i].myid);
                    settings.setValue("isgone", 1);

                settings.popTag();
            settings.popTag();
            
            settings.saveFile("settings.xml");
            
            break;
            
            
        }else{
        
            cout<<"you loose"<<endl;
            //myPrice.sort="loose";

        }
    
    }
    return myPrice;
}


void WinManager::checkWin(){
    
    int totalsuperleft=0;
    int totalspecialleft=0;
    int totalnormalleft=0;
    
    
    totalsuperleft= settings.getValue("prices:super", 0);
    totalspecialleft= settings.getValue("prices:special", 0);
    totalnormalleft= settings.getValue("prices:normal", 0);

    
    
    cout<<"checking win situation"<<endl;
    int day=ofGetDay();
    int month=ofGetMonth();

    string daystring;
    daystring+=ofToString(day,2);
    
    string monthstring="Date_";
    monthstring+=ofToString(month,2);
    
    string dateString=monthstring+"_"+daystring;
    int numdayTags = settings.getNumTags(dateString);
    
    cout<<"num Tags"<<numdayTags<<endl;

    int superprices=0;
    int specialprices=0;
    int normalprices=0;
    
    if(numdayTags > 0){
        settings.pushTag(dateString, numdayTags-1);
        superprices= settings.getValue("prices:super", 0);
        totalsuperleft-=superprices;
        specialprices= settings.getValue("prices:special", 0);
        totalspecialleft-=specialprices;
        normalprices=settings.getValue("prices:normal", 0);
        totalnormalleft-=normalprices;
        
        
        
        vector<string>win;
        
        if(totalsuperleft>0)win.push_back("prices:super");
        if(totalspecialleft>0)win.push_back("prices:special");
        if(totalnormalleft>0)win.push_back("prices:normal");
        if(win.size()>0){
        int randomIndex = rand() % win.size();
            
            
            string xmlstring=win[randomIndex];
            cout<<xmlstring<<endl;
            
            int prices= settings.getValue(xmlstring, 0);
            cout<<"there are "<<prices<<" "<<xmlstring<<endl;

            prices++;
            settings.setValue(xmlstring, prices,0);
            cout<< win[randomIndex]<< " "<<prices<<endl;


            
        }else{
            cout<<"nothing left"<<endl;
        }
        
        settings.popTag();
        settings.saveFile("settings.xml");


        
    }else{
        vector<string>win;
     win.push_back("prices:super");
        win.push_back("prices:special");
       win.push_back("prices:normal");
    int randomIndex = rand() % win.size();
        string xmlstring=win[randomIndex];
    int tagNum = settings.addTag(dateString);
        settings.setValue(dateString+":"+xmlstring, 1, tagNum);
    settings.saveFile("settings.xml");

    }
    
  

}

void WinManager::makeDalyPrices(){
      
    cout<<"make up daly prices"<<endl;
    dalyprices.clear();

    
    int day=ofGetDay();
    int month=ofGetMonth();
    
    string daystring;
    daystring+=ofToString(day,2);
    
    string monthstring="WintimesForDay_";
    monthstring+=ofToString(month,2);
    
    string dateString=monthstring+"_"+daystring;
    int numdayTags = settings.getNumTags(dateString);
    
    int totalsuperleft=0;
    int totalspecialleft=0;
    int totalnormalleft=0;
      
    int totalminutes= settings.getValue("settings:totalminutes", 840);

    
    
   // superprices= settings.getValue("prices:super", 0);

    
    if(numdayTags > 0){
        
        
        cout<<"----------- Read Prices-------------"<<endl;

        
        settings.pushTag(dateString, numdayTags-1);

        
        //lets see how many <STROKE> </STROKE> tags there are in the xml file
        int numPriceTags = settings.getNumTags("price");
        cout<<"num "<<numPriceTags<<endl;
        if(numPriceTags > 0){
            
            //we push into the last STROKE tag
            //this temporarirly treats the tag as
            //the document root.
            
            //we see how many points we have stored in <PT> tags
          //  int numPriceTags = settings.getNumTags("price");
            cout<<"num Tags"<<numPriceTags<<endl;
            
            if(numPriceTags > 0){
                for(int i = 0; i < numPriceTags; i++){
                    
                    settings.pushTag("price", i);
                    
                    if(!settings.getValue("isgone", 0)){

                        pricesStruct p;
                    p.title="hallo";
                    p.number="1218";
                    p.sort=settings.getValue("sort","normal");

                    p.wintime=settings.getValue("wintime", 0);
                    
                        p.myid=settings.getValue("myid", 0);
                        p.isGone=settings.getValue("isgone", 0);

                        
                   //     cout<<"p sort"<<settings.getValue("sort", "normal");
                     //   cout<<"p wintime"<<settings.getValue("wintime", 0);

                    dalyprices.push_back(p);
                    }
                    else{
                        cout<<"isgone"<<endl;
                    }
                        settings.popTag();
                    
                }
            }
        }

        

        
        settings.popTag();

        
        
        
        
        
    }else{
        
        
        
        totalsuperleft= settings.getValue("prices:super", 0);
        totalspecialleft= settings.getValue("prices:special", 0);
        totalnormalleft= settings.getValue("prices:normal", 0);
        
        int totalprices=totalsuperleft+totalspecialleft+totalnormalleft;
        
        int counter=0;
        
        for(int i=0;i<totalsuperleft;i++){
            
            pricesStruct p;
            p.title="hallo";
            p.number="1218";
            p.sort="super";
            p.isGone=0;
            p.myid=counter;

            int time=(int)ofRandom(0,totalminutes);
            p.wintime=time;
            dalyprices.push_back(p);
            counter++;
            
        }
        
        
        for(int i=0;i<totalspecialleft;i++){
            
            pricesStruct p;
            p.sort="special";
            p.isGone=0;
            p.myid=counter;

            int time=(int)ofRandom(0,totalminutes);
            p.wintime=time;
            dalyprices.push_back(p);
            counter++;

            
            
        }
        
        for(int i=0;i<totalnormalleft;i++){
            
            pricesStruct p;
            p.sort="normal";
            p.isGone=0;
            p.myid=counter;
            int time=(int)ofRandom(0,totalminutes);
            p.wintime=time;
            dalyprices.push_back(p);
            counter++;

            
        }
        
        int tagNum = settings.addTag(dateString);
        settings.pushTag(dateString, numdayTags-1);
        
        for(int i=0;i<dalyprices.size();i++){
            int tagNum = settings.addTag("price");
            settings.pushTag("price", i);
            
            settings.setValue("sort", dalyprices[i].sort,0);
            settings.setValue("wintime", dalyprices[i].wintime,0);
            settings.setValue("isgone", dalyprices[i].isGone,0);
            settings.setValue("myid", dalyprices[i].myid,0);

            settings.popTag();
            
        }
        settings.popTag();
        settings.saveFile("settings.xml");

    }
    
    cout<<"prices left"<<dalyprices.size()<<endl;
    
    for(int i=0;i<dalyprices.size();i++){
        cout <<dalyprices[i].wintime<<endl;
    }

    
    
}
