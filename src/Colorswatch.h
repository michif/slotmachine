//
//  Colorswatch.h
//  emptyExample
//
//  Created by Michael Flueckiger on 30.07.12.
//  Copyright (c) 2012 Ich. All rights reserved.
//



#include "ofMain.h"



class Colorswatch {
public:
    
    Colorswatch();
    virtual ~Colorswatch();
    void draw();
    void update();
    void setPosition(ofVec2f);
    void setColor(ofColor col);
    void setSecondColor(ofColor col);

    
    void setFeedbackColor(ofColor col);

    
    
    //void setFadeColor(ofColor col);
    void setFadeColor(ofColor _col, float _fadetime, bool _changecolor=false, bool _reaching=false, bool _goback=false);

    
    ofColor getSetColor();
    ofColor getActualColor();

    
    void startFadeing();
    ofColor fadeTo();
    void stopFadeing();
    bool isFadeing();
    
    void setFadeSpeed(float speed);

    
    
    float fadetime;
    bool bIsreaching;
    bool bReached;
    bool bGoback;
    

    void startFeedbacking();
    ofColor feedbacking();
    void stopFeedbacking();
   

    
    
    
    
private:
    
    ofColor myTargetColor;
    ofColor myOldColor;
    bool bIsfadeing;
    int fadeSpeed;
    int reachedValue;
    
    
    bool bColorUpdated;
    
    //color at the moment
    ofColor actualColor;
    ofColor myColor;
    
    //color of old frame
    ofColor myColorBefore;
    
    //color to get in actual frame
    ofColor myColorToBecome;
    
    //sort of Original Color;
    ofColor mySetColor;
    
    
    ofColor mySecondColor;

    

    
    float time;
    float then;
    float now;
    
    
    //feedbacking
    bool isFeedbacking();
    float feedbackfrequenzy;
    int feedbackcounter;
    
    float feedbackthen;
    
    ofColor myFeedbackColor;
    int maxfeedback;

    bool bIsfeedbacking;
    bool bFeedbackingishigh;
    
    
};






