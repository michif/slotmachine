//
//  wheel.cpp
//  slotmachine
//
//  Created by Michael Flueckiger on 12.05.15.
//
//

#include "wheel.h"


//--------------------------------------------------------------
void Wheel::setup(){
    
    
    minSpeed=100;
    maxSpeed=200;
    
    scalefact=1;
    fruitTarget=0;
    //some path, may be absolute or relative to bin/data
    string path = "icons/";
    ofDirectory dir(path);
    //only show png files
    dir.allowExt("png");
    //populate the directory object
    dir.listDir();
    maxFruits=dir.numFiles();
    
    ofImage img;
    string filePath = dir.getPath(0);
    img.loadImage(filePath);
   
    iconHeight=240;//img.height;
    wheellength=maxFruits*iconHeight;//img.height;
    breakPoint=2*iconHeight;//ofGetHeight()/2+iconHeight/2;
    stopPos= 360/2-(iconHeight/2);//((ofGetHeight()/2))-(iconHeight/2);
    
    // Das müsste in den Slotmachinecontroller, oder? 
    
    shadow.loadImage("verlauf-01.png");
    trenner.loadImage("trenner-01.png");


    float colfact=255/maxFruits;

    vector<ofColor> col;
    col.push_back(ofColor(200,144,61));//Kiwi
    col.push_back(ofColor(0,123,119));//Schraube
    col.push_back(ofColor(184,106,230));//Törtli

    col.push_back(ofColor(255,206,6));//Kleiderbügel
    col.push_back(ofColor(255,255,0));//Erdbeere
    col.push_back(ofColor(240,66,210));//Glace
    col.push_back(ofColor(230,231,232));//Wurst
    col.push_back(ofColor(53,242,63));//Ball
    col.push_back(ofColor(248,238,87));//Sonnenschirm
    col.push_back(ofColor(242,104,72));//Sonnenblume
    col.push_back(ofColor(189,143,47));//Birne
    col.push_back(ofColor(0,202,255));//Fisch

    col.push_back(ofColor(25,230,30));//Salat
    col.push_back(ofColor(0,128,199));//Besen
    col.push_back(ofColor(255,0,0));//Zopf

    col.push_back(ofColor(242,151,40));//Sandwich
   
    
    
    vector<ofColor> secondcol;
    secondcol.push_back(ofColor(55,111,194));//Kiwi
    secondcol.push_back(ofColor(255,132,136));//Schraube
    secondcol.push_back(ofColor(255,43,187));//Törtli

    secondcol.push_back(ofColor(0,49,249));//Kleiderbügel
    secondcol.push_back(ofColor(239,64,54));//Erdbeere
    secondcol.push_back(ofColor(184,106,230));//Glace
    secondcol.push_back(ofColor(25,25,25));//Wurst
    secondcol.push_back(ofColor(255,206,6));//Ball
    secondcol.push_back(ofColor(0,221,255));//Sonnenschirm
    secondcol.push_back(ofColor(13,151,183));//Sonnenblume
    secondcol.push_back(ofColor(66,112,208));//Birne
    secondcol.push_back(ofColor(255,53,0));//Fisch

    secondcol.push_back(ofColor(230,25,225));//Salat
    secondcol.push_back(ofColor(255,127,56));//Besen
    secondcol.push_back(ofColor(220,220,0));//Zopf

    secondcol.push_back(ofColor(255,0,0));//Sandwich
    
    for(int i = 0; i <maxFruits; i++){
        
        string filePath = dir.getPath(i);
        
        MovingObject o;
        fruits.push_back(o);
        fruits.back().setup();
        fruits.back().setStopPos(ofVec3f(position.x,stopPos,0));
        fruits.back().setBreakPoint(ofVec3f(position.x,breakPoint,0));
        fruits.back().setPosition(ofVec3f(0,stopPos-i*iconHeight,0));
        fruits.back().setNormalPosition(ofVec3f(0,stopPos-i*iconHeight,0));
        fruits.back().setImage(filePath);
        fruits.back().setWheellength(wheellength);
        fruits.back().setId(i);
        fruits.back().setWheelId(myId);

        
        fruits.back().setColor(ofColor(col[i]));
       fruits.back().setSecondColor(ofColor(secondcol[i]));


        
    }
    
    
    bTimerReached = false;
    startTime = ofGetElapsedTimeMillis();  // get the start time
    endTime = (int)ofRandom(1000, 5000); // in milliseconds
    bAnimationTimerReached=true;

    franklinBook14.loadFont("frabk.ttf", 25,false);
   franklinBook14.setLineHeight(18.0f);
    franklinBook14.setLetterSpacing(1.037);
    
    
    //ofAddListener(fruits[0].targetObjectReached,int,&Wheel::newEvent);



};

//--------------------------------------------------------------
void Wheel::update(){
    
    
    for(int i = 0; i <maxFruits; i++){
        fruits[i].update();
    }
    
    makeARound();
    
    
    

    // update the timer this frame
    float timer = ofGetElapsedTimeMillis() - startTime;
    if(timer >= endTime && !bTimerReached) {
        bTimerReached = true;
       
        moveToFruit(fruitTarget);
        
        // ofMessage msg("Timer Reached");
       // ofSendMessage(msg);
    }
    

     timer = ofGetElapsedTimeMillis();
    
    
    
    
    
/*
    if(timer >= startAnimationTime && !bAnimationTimerReached) {
        cout<<"animate"<<endl;
       // winningAnimation();
        bAnimationTimerReached=true;
    };
    */
    
    
    
    for(int i = 0; i <timervec.size(); i++){
        if(timer>=timervec[i].startAnimationTime && !timervec[i].bTimerReached)
        {
            fruits[getFruitTarget()].setFadeTo(timervec[i].color, timervec[i].speed);
            timervec.erase(timervec.begin() + i);
        }
    }
    
    
};


//--------------------------------------------------------------
void Wheel::draw(){
    
    ofEnableAlphaBlending();


    ofPushMatrix();
    
       // ofTranslate(0, iconHeight);//ofGetHeight()/2-iconHeight/2);


    
        //ofScale(scalefact, scalefact);
        ofTranslate(position.x, position.y);
        ofSetColor(255, 255, 255);
      
        for(int i = 0; i <maxFruits; i++){
            fruits[i].draw();
        }
        
  
    ofPopMatrix();
       ofSetColor(255);


    ofPushMatrix();
    //ofScale(scalefact, scalefact);
    ofTranslate(position.x, 0);
    ofTranslate(iconHeight, iconHeight);
    //shadow.rotate90(2);
    ofRotateZ(180);

    shadow.draw(0, 0, iconHeight,iconHeight);
    ofPopMatrix();
   
    ofPushMatrix();
    //ofTranslate(position.x, ofGetHeight()-iconHeight);
    ofTranslate(position.x, 360-iconHeight);

    shadow.draw(0, 0, iconHeight,iconHeight);
    ofPopMatrix();
    
    ofPushMatrix();
    ofTranslate(position.x-trenner.width/2, 0);
    trenner.draw(0, 0);
    ofPopMatrix();
  
    
    ofDisableAlphaBlending();
    
    typeStr=ofToString(getSpeed());
    ofPushMatrix();
    ofTranslate(position.x, 0);

    ofSetColor(0);
  //  franklinBook14.drawString(typeStr, 30, 358);
    ofPopMatrix();
    
      
};





//--------------------------------------------------------------
ofVec2f Wheel::moveToFruit(int _fruitNumber){
    
    setFruitTarget(_fruitNumber);

    
    ofVec3f stop=ofVec3f(0,stopPos,0);
    
    int fact;
    
    int offset=_fruitNumber*iconHeight;
    ofVec3f targetPosOffset=ofVec3f(0,offset,0);
    
    for(int i = 0; i <maxFruits; i++){
        fruits[i].setTargetOffset(ofVec3f(0,offset,0));
        ofVec3f normalPos=fruits[i].getNormalPosition();
        ofVec3f targetPos=fruits[i].getNormalPosition()+targetPosOffset;
        
        
        if(targetPos.y >= breakPoint){
            targetPos.y=-wheellength+targetPos.y;
        }
      
        if(targetPos.y < fruits[i].getPosition().y){
          targetPos.y+=wheellength;//+breakPoint+stop.y;


        }else{
        }
        fruits[i].setRecalcTarget(true);
        fruits[i].moveTo(targetPos);

        
    }


}



//--------------------------------------------------------------
ofVec2f Wheel::moveToFruitWithTimeOffsett(int _fruitNumber, float _milliseconds){
    
    setFruitTarget(_fruitNumber);
    startTime = ofGetElapsedTimeMillis();  // get the start time
    endTime = _milliseconds; // in milliseconds
    bTimerReached = false;
    
    int mySpeed=ofRandom(minSpeed,maxSpeed);
    for(int i = 0; i <maxFruits; i++){
       fruits[i].setMaxSpeed(mySpeed);
    }
    
    startRolling(mySpeed);
   }




void Wheel::startWinningAnimationWithTimeOffset(int millis, ofColor col=ofColor(255,0,0),float speed){
    startAnimationTime = millis+ofGetElapsedTimeMillis();
    startWinningAnimation();
    Timercomponent t;
    t.millis=millis;
    t.startAnimationTime=millis+ofGetElapsedTimeMillis();
    t.bTimerReached=false;
    t.color=col;
    t.speed=speed;
    timervec.push_back(t);
    
}


    void Wheel::startWinningAnimation(){

        bAnimationTimerReached=false;
    
}


    void Wheel::stopWinningAnimation(){
   
        timervec.clear();
        
    
}

    void Wheel::winningAnimation(){

  //  if(now==startAnimationTime){
        fruits[getFruitTarget()].setFadeTo(fruits[getFruitTarget()].getSecondColor(), 0.2);

   // }
    
    
}


ofColor Wheel::getTargetFruitColor(){
    return fruits[getFruitTarget()].getColor();
}

ofColor Wheel::getTargetFruitSecondColor(){
    return fruits[getFruitTarget()].getSecondColor();
}

//Helper
//--------------------------------------------------------------

void Wheel::setAllStops(){
       
}


//--------------------------------------------------------------

void Wheel::startRolling(float speed){
  //  fruitTarget=-1;
    
    for(int i = 0; i <maxFruits; i++){
        fruits[i].setPosition(fruits[i].getNormalPosition());
        fruits[i].startMove();
        fruits[i].setSpeed(ofVec3f(0,speed,0));
    }


}

void Wheel::setSpeed(int _minSpeed, int _maxSpeed){
    minSpeed=_minSpeed;
    maxSpeed=_maxSpeed;
    
    
    for(int i = 0; i <maxFruits; i++){
        fruits[i].setMaxSpeed(minSpeed);
    }
    
    
    
}





//Helper
//--------------------------------------------------------------

void Wheel::makeARound(){
    
    for(int i = 0; i <maxFruits; i++){
        if(fruits[i].getPosition().y>breakPoint){
            fruits[i].madeARound();
            fruits[i].setPosition(ofVec3f(fruits[i].getPosition().x,-wheellength+breakPoint,fruits[i].getPosition().z));
        }
    }
}


void Wheel::setPosition(ofVec3f _position){
    position.set(_position);

}


void Wheel::reset(){

    for(int i = 0; i <maxFruits; i++){
        fruits[i].setPosition(ofVec3f(0,stopPos-i*iconHeight,0));
    }
}


ofVec2f Wheel::getPosition(){
    return position;

}

float Wheel::getSpeed(){
        spinningSpeed=fruits[fruitTarget].getSpeed().y;
    return spinningSpeed;
}



void Wheel::setFruitTarget(int _fruitTarget){
    fruitTarget=_fruitTarget;
    
    for(int i=0;i<fruits.size();i++){
        fruits[i].setIsStopingIcon(false);
    }
     fruits[fruitTarget].setIsStopingIcon(true);
}

int Wheel::getFruitTarget(){
    return fruitTarget;
}


void Wheel::setId( int _id){
    myId=_id;
}

int Wheel::getId(){
    return myId;
}



void Wheel::newEvent(int & i){
    cout<<"Event "<<i<<endl;
}

